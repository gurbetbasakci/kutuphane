﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(kitap.Startup))]
namespace kitap
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
